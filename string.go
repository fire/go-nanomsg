package nanomsg

// #include <nanomsg/nn.h>
// #cgo LDFLAGS: -lnanomsg
import "C"

func (soc *Socket) RecvString(flags int) (string, error) {
	b, err := soc.Recv(flags)
	n, err := soc.RecvBuffer()
	s := string(b[:n])
	return s, err
}

